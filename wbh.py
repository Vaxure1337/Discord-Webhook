import requests

btc = 'https://api.coindesk.com/v1/bpi/currentprice/BTC.json'
data = requests.get(btc).json()
price_us = data['bpi']['USD']['rate']

discord = "webhook"
payload = {
    "username": "Adax",
    "avatar_url": "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Ftechcrunch.com%2Fwp-content%2Fuploads%2F2014%2F01%2Fgrowth-hacker1.jpg&f=1",
    "content": "Bitcoin Value: " + price_us
}

requests.post(discord, data=payload)
